let express = require('express');
let router = express.Router();
let axios = require('axios');
let querystring = require('querystring');

let cache = {};

const TTL = 10000;

function getServer() {
  return "len-kgis-gis";
}

function getAdditionalInfo(params) {
  let {id, layer} = params.layer;

  let data = {};
  return data;
}

function getAdditionalFields(params) {
  return params;
}

function addTaskInfo(feature) {
  return feature;
}

router.get('/info', (req, res) => {
  let server = req.params.server;
  let info = {
    "currentVersion": 10.5,
    "fullVersion": "10.5.0",
    "soapUrl": "http://" + server + ":3000/rest/services",
    "secureSoapUrl": "http://" + server + ":3000/rest/services",
    "authInfo": {
      "isTokenBasedSecurity": false,
      "tokenServicesUrl": "http://" + server + ":3000/rest/info/",
      "shortLivedTokenValidity": 60
    }
  };
  res.json(info);
});

router.get("/services/:catalog/:service/FeatureServer/:id", (req, res) => {
  let server = getServer();
  let catalog = req.params.catalog;
  let service = req.params.service;
  let id = req.params.id;

  let url = ["http:/", server, "arcgis/rest/services", catalog, service, "FeatureServer", id].join("/") + "?f=json";
  let key = url;
  axios.get(url).then((dataSet) => {
    let data = dataSet.data;
    data = getAdditionalFields(data);
    cache[key] = data;
    res.json(cache[key]);
  })
});

function requestUrl(url, key, res) {
  axios.get(url)
    .then((dataSet) => {
      if (dataSet && dataSet.data)
      {
        cache[key] = dataSet.data;
        cache[key].timestamp = Date.now();
        res.json(cache[key]);
      }
      else
      {
        console.log(`> кэш не содержит данные запроса`)
        res.json(dataSet.data);
      }
    })
    .catch((e) => {
      res.json({status: false});
    })
}

router.get("/services/:catalog/:service/FeatureServer/:id/query", (req, res) => {
  let server = getServer();
  let catalog = req.params.catalog;
  let service = req.params.service;
  let id = req.params.id;
  let query = req.query;

  let url = ["http:/", server, "arcgis/rest/services", catalog, service, "FeatureServer", id, "query"].join("/") + "?";

  url += querystring.stringify(query);

  let key = url;
  requestUrl(url, key, res);
});

router.post('/services/:catalog/:service/FeatureServer/:id/applyEdits', (req, res) => {
  res.json({})
})

router.post('/services/:catalog/:service/FeatureServer/:id/addFeatures', (req, res) => {
  res.json({})
})

router.post('/services/:catalog/:service/FeatureServer/:id/updateFeatures', (req, res) => {
  res.json({})
})
router.post('/services/:catalog/:service/FeatureServer/:id/deleteFeatures', (req, res) => {
  res.json({})
})

module.exports = router;
